/****************************************************************************
 
  Header file for Spirometer 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Spirometer_H
#define Spirometer_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    
#include "ADMulti.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPState_Spirometer,
               SpirometerStandby,
               SpirometerActive} SpirometerState_t ;

// Public Function Prototypes
bool IsBreathStrong(uint32_t);
bool CheckSpirometerBreath(void);
bool InitSpirometerSM(uint8_t);
bool PostSpirometerSM(ES_Event_t);
ES_Event_t RunSpirometerSM(ES_Event_t);

#endif /* CoralReefService_H */

