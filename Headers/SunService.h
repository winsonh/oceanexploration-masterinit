/****************************************************************************

  Header file for SunService
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/
#ifndef SunService_H
#define SunService_H

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    /*gets event types */


// typedefs for the states
// State definitions for use with the query function
typedef enum {  InitSun, Ticking }SunState_t;

// Public Function Prototypes

bool InitializeSunService(uint8_t Priority);
bool PostSunService(ES_Event_t ThisEvent);
ES_Event_t RunSunService(ES_Event_t ThisEvent);


static bool TickSun();
static bool InitSunPWMPorts(void);
#endif /* SunServie_H */
