/****************************************************************************
 Module
   CoralReefService.c

 Revision
   1.0.1

 Description
   #################################################################
   #################################################################
   #################################################################
   #################################################################
   This module includes the functions 

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/11/18 12:22 win      Implement RunCoralReefSM function
 11/09/18 17:33 win      Started to write CoralReefService.c
****************************************************************************/

// #define TEST

/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "CoralReefService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in the final project that are referenced
*/
#include "MasterService.h"


/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define NUM_AD_INPUT_PORTS 4
#define MAX_AD_COUNTS 4096
#define PWM_FREQ 50
#define PE1 1
#define WHITE_LED_PIN 4 
#define COLOR_LED_PIN 5
#define PWM_CHANNEL_GROUP 2
#define ADCountsResolution 30

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static bool InitPWMPorts(void);
static bool OutputPWM(uint8_t DutyCycle, uint8_t PinNum);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static CoralReefState_t CurrentState = InitPState_CoralReef;
// static ES_Event_t DeferralQueue[3+1];
static uint32_t ADResults[NUM_AD_INPUT_PORTS];
static uint16_t HighestADCounts;
static uint16_t InitialADCountsDiff;
static bool GameCompletion = false;
/*------------------------------ Module Code ------------------------------*/





/****************************************************************************
 Function
     InitCoralReefSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     This function initializes 
        (a) Analog-to-Digital channel PE1 (Channel 1)
        (b) PWM ports PE4 and PE5 (Channel 4 and Channel 5)

     If initialization is successful,
         (a) CurrentState will enter FullColor
         (b) An ES_INIT event will be passed to CoralReef Service
         (c) This function returns true

     If initializatio nis unsuccessful, 
         (a) otherwise, an ES_ERROR event will be passed to
             CoralReef Service.
         (b) This function returns false

 Author
     Winson Huang, 11/09/18, 17:44
****************************************************************************/
bool InitCoralReefSM ( uint8_t Priority )
{
  
  ES_Event_t ThisEvent; 
  
  MyPriority = Priority;
	CurrentState = InitPState_CoralReef;

  // Analog Port Initialization (PE1, but have to enable all ADC)
  ADC_MultiInit(4); 
  
  // PWM Port Initialization (PE4 and PE5)
  if (InitPWMPorts() != true) {
    ThisEvent.EventType = ES_ERROR;
    return PostCoralReefSM(ThisEvent);
  }

  // Acquire the value for InitialTemperatureCountDiff
  // Update ADResults with the current analog inputs, PE1 = ADResults[1]
  ADC_MultiRead(ADResults); 
  InitialADCountsDiff = MAX_AD_COUNTS - ADResults[PE1];
  
  
  
  
  
  /*
  // Analog Port Read
  ADC_MultiRead(ADResults);
  printf(" Ch0 = %u, Ch1 = %u, Ch2 = %u, Ch3 = %u \n\r", ADResults[0], ADResults[1], ADResults[2], ADResults[3]);
  // */
  
   
  ThisEvent.EventType = ES_INIT;
  CurrentState = FullColor;
  
  printf("\n\rInitCoralReefService is successfully initialized.");
  return PostCoralReefSM(ThisEvent);
}







/****************************************************************************
 Function
     InitPWMPorts

 Parameters
     None

 Returns
     bool, 

 Description
     This function initializes 
        (a) Analog-to-Digital channel PE1 (Channel 1)
        (b) PWM ports PE4 and PE5 (Channel 4 and Channel 5)

     If initialization is successful,
         (a) CurrentState will enter FullColor
         (b) An ES_INIT event will be passed to CoralReef Service
         (c) This function returns true

     If initializatio nis unsuccessful, 
         (a) otherwise, an ES_ERROR event will be passed to
             CoralReef Service.
         (b) This function returns false

 Author
     Winson Huang, 11/09/18, 17:44
****************************************************************************/
static bool InitPWMPorts(void) {
  
  // PWM Ports Initialization (PE4 and PE5)
  if(PWM_TIVA_Init(16) != true){
    printf("\n\rFailed PWM_TIVA_Init(%d)", (COLOR_LED_PIN + 1));
    return false;
  }else{
    printf("\n\rPassed PWM_TIVA_Init(%d)", (COLOR_LED_PIN + 1));
  }
  
  // Set PWM Frequency
  if(PWM_TIVA_SetFreq(PWM_FREQ, PWM_CHANNEL_GROUP) != true) {
    printf("\n\rFailed PWM_TIVA_SetFreq(%d, %d)", PWM_FREQ, PWM_CHANNEL_GROUP);
    return false;
  }
  
  // Set Duty Cycle
  OutputPWM(0, WHITE_LED_PIN);
  OutputPWM(100, COLOR_LED_PIN);
  
  return true;
}





/*
*/
static bool OutputPWM(uint8_t DutyCycle, uint8_t PinNum) {
  
  if(PWM_TIVA_SetDuty( DutyCycle, PinNum) != true){
    printf("\n\rFailed PWM_TIVA_SetDuty( %d, %d)", DutyCycle, PinNum);
    return false;
  }
  
  printf("\n\rPWM_TIVA_SetDuty( %d, %d)", DutyCycle, PinNum);
  
  return true;
}










































ES_Event_t RunCoralReefSM(ES_Event_t ThisEvent) {
  struct ES_Event ReturnEvent;
  
  switch (CurrentState) {
    case FullColor:
      if(ThisEvent.EventType == ES_TEMPERATURE_RISE) {
        CurrentState = ColorTransitioning;
        uint16_t CurrentADCountsDiff = ThisEvent.EventParam - (MAX_AD_COUNTS - InitialADCountsDiff);
        uint8_t WhiteDutyCycle = (uint8_t)(100.0 * (float)CurrentADCountsDiff / (float)InitialADCountsDiff);
        uint8_t ColorDutyCycle = 100 - WhiteDutyCycle;
        
        // Output PWM for the current duty cycle
        OutputPWM(WhiteDutyCycle, WHITE_LED_PIN);
        OutputPWM(ColorDutyCycle, COLOR_LED_PIN);
      }
      break;
    
    
    case ColorTransitioning:
      if(ThisEvent.EventType == ES_TEMPERATURE_RISE) {
        uint16_t CurrentADCountsDiff = ThisEvent.EventParam - (MAX_AD_COUNTS - InitialADCountsDiff);
        uint8_t WhiteDutyCycle = (uint8_t)(100.0 * (float)CurrentADCountsDiff / (float)InitialADCountsDiff);
        uint8_t ColorDutyCycle = 100 - WhiteDutyCycle;
        
        // If duty cycle > 98%, enter White state
        if(WhiteDutyCycle >= 98) {
          if(GameCompletion == false) {
            OutputPWM(100, WHITE_LED_PIN);
            OutputPWM(0, COLOR_LED_PIN);
            
            printf("\n\n\r*****\n\rPost ES_CORALREEF_GAME_WON to the main state machine\n\r*****\n");
            ThisEvent.EventType = ES_CORALREEF_GAME_WON;
            PostMasterSM(ThisEvent); // Post to the main state machine
            GameCompletion = true;
      }
          CurrentState = White; 
        }
        
        // Output PWM for the current duty cycle
        OutputPWM(WhiteDutyCycle, WHITE_LED_PIN);
        OutputPWM(ColorDutyCycle, COLOR_LED_PIN);
      }
      break;
      
      
    case White:

      break;
  }
  
    
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}


































/****************************************************************************
 Function
     CheckGlobalWarming

 Parameters
     None

 Returns
     bool, true if a rise in temperature is detected, false otherwise

 Description
     This event checker function checks for whether the current 
     temperature is higher than the highest temperature previously
     recorded. 
     If a higher temperature is detected, 
         (a) This function returns true
         (b) This function passes an ES_TEMPERATURE_RISE event
             to CoralReefService
     Otherwise,
         (a) This function returns false.
 
 Author
     Winson Huang, 11/09/18, 19:05
****************************************************************************/
bool Check4GlobalWarming(void) {
  // printf("\n\rCheck4BlobalWarming");
  
  bool ReturnVal = false;
  uint16_t CurrentADCounts = 0;
  
  // Update ADResults with the current analog inputs, PE1 = ADResults[1]
  ADC_MultiRead(ADResults); 
  CurrentADCounts = ADResults[PE1]; 
  // printf("\n\rCurrentADCounts = %d", CurrentADCounts);
  // If a rise in temperature is detected
  if(CurrentADCounts > (HighestADCounts + ADCountsResolution)) {
    ReturnVal = true;
    HighestADCounts = CurrentADCounts;
    printf("\n\rNew HighestADCounts = %d", HighestADCounts);
    
    // When a rise temperature is detected, reset and restart the 30-s timer.
    ES_Timer_StopTimer(TIMER_30s);
    ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
    ES_Timer_StartTimer(TIMER_30s);
    
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_TEMPERATURE_RISE; 
    ThisEvent.EventParam = CurrentADCounts;
    
    PostCoralReefSM(ThisEvent); 
  }
  
 
  return ReturnVal;
}








































/****************************************************************************
 Function
     PostCoralReefSM

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Author
     Winson Huang, 11/09/18, 19:47
****************************************************************************/
bool PostCoralReefSM( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}













/****************************************************************************
                                Test Harness
****************************************************************************/
#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  // printf("TEST NOW!\n");
  InitCoralReefService (MyPriority);
  
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  
  while(!kbhit()) {
    if(Check4GlobalWarming()) {
      printf("\n\rTemperature Rises!");
    }
  }
  
  return 0;
}

#endif


