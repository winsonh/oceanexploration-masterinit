/*FishLED_Service*/

#include "FishLED_Service.h"
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "PWM16Tiva.h"

/*TivaLibrary*/
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the other modules in Lab3 that are referenced
*/
//#include "LCD_Write.h"
//#include "LCDService.h"


//#define ALL_BITS (0xff << 2)

/*---------------------------- Module Variables ---------------------------*/
static uint8_t      MyPriority;
static FishWheelStates_t  CurrentState;
static int SelectedFish = 0;





static uint8_t LastButtonState;
static uint8_t LastLimitState;

/*static uint8_t A2HI = BIT2HI;
static uint8_t A3HI = BIT3HI;
static uint8_t A4HI = BIT4HI;
static uint8_t A5HI = BIT5HI;
static uint8_t A6HI = BIT6HI;
//static uint8_t A7HI = BIT7HI;

static uint8_t A2LO = BIT2LO;
static uint8_t A3LO = BIT3LO;
static uint8_t A4LO = BIT4LO;
static uint8_t A5LO = BIT5LO;
static uint8_t A6LO = BIT6LO;
//static uint8_t A7LO = BIT7LO;


//static uint8_t C4HI = BIT4HI;
//static uint8_t C5HI = BIT5HI;
static uint8_t C6HI = BIT6HI;
//static uint8_t C7HI = BIT7HI;

static uint8_t C4LO = BIT4LO;
static uint8_t C5LO = BIT5LO;
static uint8_t C6LO = BIT6LO;
//static uint8_t C7LO = BIT7LO;
*/

//Function Prototypes
ES_Event_t RunFishWheel(ES_Event_t ThisEvent);
void TurnOffAllFishLed(void);
void TurnOnLED1(void);
void TurnOnLED2(void);
void TurnOnLED3(void);
/*-----------------Functions-InitalizeFishWheel----------------*/

bool InitalizeFishWheel(uint8_t Priority)
{
  puts("Enter Fish Init");
//	printf("initialize\n");
//Takes a priority number, returns True.
  ES_Event_t ThisEvent;
//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;

  //TERMIO_Init();
  //PA6 (10), PA7 (11), PC4 (6), PC5 (7)
  PWM_TIVA_SetDuty(100, 10);
  HWREG(SYSCTL_RCGCGPIO) |= (SYSCTL_RCGCGPIO_R0 | SYSCTL_RCGCGPIO_R2); //enable port A and C
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2) //while loop for A
  {}

  //HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (A2HI|A3HI|A4HI|A5HI|A6HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  // enable digital bits 2-7 on port A
  //HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (A2LO|A3LO|A4LO|A5LO|A6HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT2LO & BIT3LO & BIT4LO & BIT5LO);
  //HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT6HI | BIT7HI);
    PWM_TIVA_SetDuty(100, 10); //A6
    PWM_TIVA_SetDuty(100, 11); //A7
    //set  inputs for bits 2-5, and outputs for 6

  //HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (C4HI|C5HI|C6HI);
  //  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT4HI | BIT5HI | BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT6HI);
    PWM_TIVA_SetDuty(100, 6); //C4
    PWM_TIVA_SetDuty(100, 7); //C5
  // enable digital bits 4-7 on port C
  //HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT6HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT6HI);
  //set  inputs for bits 4-7 output 
    
  //SetValues to 0
  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT2LO & BIT3LO & BIT4LO & BIT5LO);
      PWM_TIVA_SetDuty(0, 10); //A6
    PWM_TIVA_SetDuty(0, 11); //A7
    
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO & BIT5LO & BIT6LO);
   PWM_TIVA_SetDuty(0, 6); //C4
    PWM_TIVA_SetDuty(0, 7); //C5
    
  /*  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT7HI | BIT6HI);
    //puts("Set A bits High");
    
    HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT7LO & BIT6LO);
    //puts("Set A bits LO");
  
    HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI | BIT5HI | C6HI);  
    //puts("Set C bits high");
    
    HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (C4LO & C5LO & C6LO);  
    //puts("Set C bits Low");
  */  
  //  printf( "%d", HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)));
    
  LastLimitState  = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI | BIT3HI | BIT4HI);
  LastButtonState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI);
  CurrentState = InitFishWheel;
    
  // Potentially wrong
    
   // CurrentState = InitFishWheel;

  ThisEvent.EventType = ES_INIT;

  if (ES_PostToService(MyPriority, ThisEvent) == 1)
  {
    printf("initialize posted\n");
    return 1;
  }
  else
  {
    return 0;
    printf("initialize not posted");
  }
  //return 1;
}

/*----------------------Functions PostMorseElement-----------------------*/
bool PostFishWheel(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*----------------------Functions CheckMorseEvents-----------------------*/

bool CheckFishWheelEvents(void)
{
  //puts ("entered CheckFishWheelEvents");
//Takes no parameters, returns True if an event was posted (11/4/11 jec)
  ES_Event_t  ThisEvent;
  bool        ReturnVal;
  ReturnVal = 0;
  
  uint8_t CurrentLimitState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI | BIT3HI | BIT4HI);
  uint8_t CurrentButtonState = HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI);
  
  if (CurrentButtonState != LastButtonState)
  {
   // printf("CurrentButtonState: %d NewButtonState:%d \r\n", CurrentButtonState, LastButtonState);
    if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT5HI)) != 0)
    {
     puts ("button press event event");
     ThisEvent.EventType =  ES_FISH_BUTTON_PRESS;
     ES_PostToService(MyPriority, ThisEvent);
    }
    
    else //if (A5_val == A5LO)
    {
     puts ("button unpress event \r\n"); 
     ThisEvent.EventType =  ES_FISH_BUTTON_UNPRESS;
     ES_PostToService(MyPriority, ThisEvent);
    }
    ReturnVal = 1;
  }
  
  else if (CurrentLimitState != LastLimitState)
  {
    //puts("new limit state entered");
    //printf("CurrentLimState: %d NewLimState:%d \r\n", CurrentLimitState, LastLimitState);
    if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT2HI))!= 0)
    {
     puts ("lim 1 press event \r\n");
      TurnOnLED1();
     ThisEvent.EventType =  ES_LIMIT_1;
     ES_PostToService(MyPriority, ThisEvent);
    }
    
    else if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT3HI)) != 0)
    {
     puts ("lim 2 press event \r\n");
      TurnOnLED2();
     ThisEvent.EventType =  ES_LIMIT_2;
     ES_PostToService(MyPriority, ThisEvent);
    }
    
    else if ((HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT4HI)) != 0)
    {
     puts ("lim 3 press event \r\n");
      TurnOnLED3();
     ThisEvent.EventType =  ES_LIMIT_3;
     ES_PostToService(MyPriority, ThisEvent);
    }
    ReturnVal = 1;
    
  }
  LastLimitState = CurrentLimitState;
  LastButtonState = CurrentButtonState;
  return ReturnVal;
}

void TurnOffAllFishLed(void){
  //puts("in turn off all fish");
  
  PWM_TIVA_SetDuty(0, 10); //A6
  PWM_TIVA_SetDuty(0, 11); //A7
  PWM_TIVA_SetDuty(0, 6); //C4
  
  //HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT7LO & BIT6LO);
  //HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO);
}

void TurnOnLED1(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 10); //A6
  //HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT6HI);
}

void TurnOnLED2(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 11);
  //HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT7HI);
}

void TurnOnLED3(void){
  TurnOffAllFishLed();
  PWM_TIVA_SetDuty(100, 6);
  //HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT4HI);
}

void TurnOnGreenLed(void){
  //HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT5HI);
  PWM_TIVA_SetDuty(100, 7);
  //HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT6HI); //test red in green
}

void TurnOffGreenLed(void){
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT5LO);
  PWM_TIVA_SetDuty(0, 7);
}

void TurnOnRedLed(void){
  printf("turn on red light \r\n");
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT6HI); //test red
  printf ("red should be def on \r\n");
}

void TurnOffRedLed(void){
  HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT6LO);
}


ES_Event_t RunFishWheel(ES_Event_t ThisEvent){
  //puts ("Enter Run");
  
  ES_Event_t NoEventReturn;
  //static FishWheelStates_t  CurrentState;  
  FishWheelStates_t  NextState;
  NextState = CurrentState;
  printf( "Current State: %d \r\n", CurrentState);
  switch (CurrentState)
    {
      case InitFishWheel:
      {
        puts ("Inside InitFishWheel State");
        //puts(" in InitFISHElements \n");
        //TurnOffAllFishLed();
        
        SelectedFish = 0;
        //puts(" Past selected Fish \n");
       
        if (ThisEvent.EventType == ES_FISH_BUTTON_PRESS)
        {
          puts("ES_FISH_BUTTON_PRESS \n");
          NextState = FishButtonDown;
        }
        else if (ThisEvent.EventType == ES_LIMIT_1)
        {
          puts("ES_LIMIT_1 \n");
          NextState = LedFish1; //
        }
        else if (ThisEvent.EventType == ES_LIMIT_2)
        {
          puts("ES_LIMIT_2 \n");
          NextState = LedFish2;
        }
        else if (ThisEvent.EventType == ES_LIMIT_3)
        {
          puts("ES_LIMIT_3 \n");
          NextState = LedFish3;
        }
      }
      break;
      case LedFish1:
      {
        puts ("LedFish1State");
        TurnOffRedLed();
        
        //TurnOnLED1();
        SelectedFish = 1;
        if (ThisEvent.EventType == ES_FISH_BUTTON_PRESS)
        {
          NextState = FishButtonDown;
        }
        else if (ThisEvent.EventType == ES_LIMIT_2)
        {
          //puts("ES_INIT \n");
          NextState = LedFish2;
        }
        else if (ThisEvent.EventType == ES_LIMIT_3)
        {
          //puts("ES_INIT \n");
          NextState = LedFish3;
        }
        else if (ThisEvent.EventType == ES_LIMIT_1)
        {
          //puts("ES_INIT \n");
          NextState = LedFish1;
        }
      }
      break;
      case LedFish2:
      {
        puts ("LedFish2State");
        TurnOffRedLed();

        //TurnOnLED2();
        SelectedFish = 2;
        if (ThisEvent.EventType == ES_FISH_BUTTON_PRESS)
        {
          NextState = FishButtonDown;
        }
        else if (ThisEvent.EventType == ES_LIMIT_1)
        {
          //puts("ES_INIT \n");
          NextState = LedFish1;
        }
        else if (ThisEvent.EventType == ES_LIMIT_3)
        {
          //puts("ES_INIT \n");
          NextState = LedFish3;
        }
        else if (ThisEvent.EventType == ES_LIMIT_2)
        {
          //puts("ES_INIT \n");
          NextState = LedFish2;
        }
      }
      break;
      case LedFish3:
      {
        puts ("Inside LedFish3State");
        TurnOffRedLed();
        //TurnOnLED3();
        SelectedFish = 3;
        if (ThisEvent.EventType == ES_FISH_BUTTON_PRESS)
        {
          NextState = FishButtonDown;
        }
        else if (ThisEvent.EventType == ES_LIMIT_2)
        {
          //puts("ES_INIT \n");
          NextState = LedFish2;
        }
        else if (ThisEvent.EventType == ES_LIMIT_1)
        {
          //puts("ES_INIT \n");
          NextState = LedFish1;
        }
        else if (ThisEvent.EventType == ES_LIMIT_3)
        {
          //puts("ES_INIT \n");
          NextState = LedFish3;
        }
      }
      break;
      case FishButtonDown:
      {
        puts ("Inside FishButtonDown");
        if (SelectedFish == 0)
        {
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            NextState = InitFishWheel;
          }
        }
        else if (SelectedFish == 1)
        {
          TurnOnRedLed();
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            puts (" red button unpress just occured");
            //TurnOffRedLed();
            NextState = LedFish1;
          }
        }
        else if (SelectedFish == 2)
        {
          TurnOnRedLed();
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            //TurnOffRedLed();
            NextState = LedFish2;
          }
        }
        else if (SelectedFish == 3)
        {
          TurnOnGreenLed(); //WINNER
          ThisEvent.EventType = ES_FISH_GAME_WON;
          puts("fish game won");
          //PostFishGameWon;
          if (ThisEvent.EventType == ES_FISH_BUTTON_UNPRESS)
          {
            TurnOffGreenLed();
            NextState = InitFishWheel;
          }
        }
      }
      break;
    }
    CurrentState = NextState;
    //puts("changing current state to next state \r\n");
    NoEventReturn.EventType = ES_NO_EVENT;
    //ThisEvent.EventType = ES_NO_EVENT;
    return NoEventReturn;
}

