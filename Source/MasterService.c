/****************************************************************************
 Module
   MasterService.c

 Revision
   1.0.1

 Description
   This module serves as the master service governing the start and the end 
   of TREE, as well as tracking the progress of each sub-game.  

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/12/18 16:33 win      Started to write MasterService.c
****************************************************************************/

// #define TEST

/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "MasterService.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in the final project that are referenced
*/


/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/



/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static MasterState_t CurrentState = InitPState_Master;
// static ES_Event_t DeferralQueue[3+1];
static uint8_t GameWonRecord = 0; // Bit0: Fish Matching. Bit1: Coral Reef. Bit2: Whale Blow 
/*------------------------------ Module Code ------------------------------*/





/****************************************************************************
 Function
     InitMasterSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
 	 This function initializes the required data for the Master service.

 Author
     Winson Huang, 11/12/18, 16:44
****************************************************************************/
bool InitMasterSM ( uint8_t Priority )
{
  ES_Event_t ThisEvent; 
  
  MyPriority = Priority;
  CurrentState = InitPState_Master;
   
  ThisEvent.EventType = ES_INIT;
  // CurrentState = LEAFOut_Welcoming;
  CurrentState = LEAFIn_ZeroWon;
  
  // Begin the 30s Timer
  printf("Timer starts");
  ES_Timer_SetTimer(TIMER_30s, 30 * ONE_SEC);
  ES_Timer_StartTimer(TIMER_30s);
  
  // We might want to do all the hardware initialization here.

  return PostMasterSM(ThisEvent);
}


















ES_Event_t RunMasterSM(ES_Event_t ThisEvent) {
  struct ES_Event ReturnEvent;
  
  
  switch (CurrentState) {
    
    // LEAFOut_Welcoming state
    case LEAFOut_Welcoming:
      if(ThisEvent.EventType == ES_LEAF_INSERTION) {
        CurrentState = LEAFIn_ZeroWon;
      }
      break;

      
    // LEAFIn_ZeroWon state 
    case LEAFIn_ZeroWon:

      if(ThisEvent.EventType == ES_FISH_GAME_WON) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = LEAFIn_OneWon;
        }
      }
      else if(ThisEvent.EventType == ES_CORALREEF_GAME_WON) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = LEAFIn_OneWon;
          printf("\n\r ES_CORALREEF_GAME_WON, GameWonRecord = %d", GameWonRecord);
        }
      }
      else if(ThisEvent.EventType == ES_WHALE_GAME_WON) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = LEAFIn_OneWon;
        }
      }
      else if(ThisEvent.EventType == ES_TIMEOUT) {
        printf("\n\rMasterSM receives ES_TIMEOUT");
        ES_Timer_StopTimer(TIMER_30s);
        // Tell every service to go back to their initial state
        // Set MasterSM back to the initial state.
      }
      break;
    
    
    // LEAFIn_OneWon state 
    case LEAFIn_OneWon:
      if(ThisEvent.EventType == ES_FISH_GAME_WON) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = LEAFIn_TwoWon;
        }
      }
      else if(ThisEvent.EventType == ES_CORALREEF_GAME_WON) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = LEAFIn_TwoWon;
        }
      }
      else if(ThisEvent.EventType == ES_WHALE_GAME_WON) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = LEAFIn_TwoWon;
        }
      }
      else if(ThisEvent.EventType == ES_TIMEOUT) {
        printf("\n\rMasterSM receives ES_TIMEOUT");
        ES_Timer_StopTimer(TIMER_30s);
        // Tell every service to go back to their initial state
        // Set MasterSM back to the initial state.
      }
            
      
      break;
    
    
    // LEAFIn_TwoWon state
    case LEAFIn_TwoWon:
      if(ThisEvent.EventType == ES_FISH_GAME_WON) {
        if( (GameWonRecord & BIT0HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT0HI;
          CurrentState = LEAFIn_AllWon;
          
          ES_Event_t ThisEvent;
          ThisEvent.EventType = ES_ALL_WON;
          PostMasterSM(ThisEvent);
        }
      }
      else if(ThisEvent.EventType == ES_CORALREEF_GAME_WON) {
        if( (GameWonRecord & BIT1HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT1HI;
          CurrentState = LEAFIn_AllWon;
          
          ES_Event_t ThisEvent;
          ThisEvent.EventType = ES_ALL_WON;
          PostMasterSM(ThisEvent);
        }
      }
      else if(ThisEvent.EventType == ES_WHALE_GAME_WON) {
        if( (GameWonRecord & BIT2HI) == 0 ) {
          GameWonRecord = GameWonRecord | BIT2HI;
          CurrentState = LEAFIn_AllWon;
          
          ES_Event_t ThisEvent;
          ThisEvent.EventType = ES_ALL_WON;
          PostMasterSM(ThisEvent);
        }
      }
      else if(ThisEvent.EventType == ES_TIMEOUT) {
        printf("\n\rMasterSM receives ES_TIMEOUT");
        ES_Timer_StopTimer(TIMER_30s);
        // Tell every service to go back to their initial state
        // Set MasterSM back to the initial state.
      }
      
      break;
    

    // LEAFIn_AllWon state        
    case LEAFIn_AllWon:
      // Tube Guy Dance
      // Stamp LEAF
      // Make all the services to the welcoming state.
      CurrentState = LEAFIn_ZeroWon;
      GameWonRecord = 0;
      break;
  }

  
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}































/****************************************************************************
 Function
     PostMasterSM

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

 Author
     Winson Huang, 11/12/18, 16:52
****************************************************************************/
bool PostMasterSM( ES_Event_t ThisEvent )
{ 
  return ES_PostToService( MyPriority, ThisEvent);
}










/****************************************************************************
                                Test Harness
****************************************************************************/
#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  // printf("TEST NOW!\n");
  InitCoralReefService (MyPriority);
  
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  printf("\n\rTEST NOW!");
  
  while(!kbhit()) {
    if(Check4GlobalWarming()) {
      printf("\n\rTemperature Rises!");
    }
  }
  
  return 0;
}

#endif
