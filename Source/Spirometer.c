/****************************************************************************
 Module
   Spirometer.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Spirometer.h"

/*----------------------------- Module Defines ----------------------------*/
#define STRONG_BREATH 4000
#define ONE_SEC 1000
#define PE0 0
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static SpirometerState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static uint16_t CurrentInputState;
static uint32_t ADCResults[4];
static uint16_t BaselineADCResult;
static bool GameCompletion = false;
static bool ReturnVal = false;
static ES_Event_t ThisEvent;

/*------------------------------ Module Code ------------------------------*/
bool IsBreathStrong(uint32_t ADC_Count) {
  bool result = false;
  if (ADC_Count > STRONG_BREATH) {
    // printf("Breath: %d\n\r", ADC_Count);
    result = true;
  }
  return result;
}

bool CheckSpirometerBreath(void){
  //Get current input state from the port line
  ReturnVal = false;
  
  ADC_MultiRead(ADCResults);
  CurrentInputState = ADCResults[PE0];
  
  if(IsBreathStrong(CurrentInputState)) {
    ThisEvent.EventType = ES_BREATH_DETECTED;
    PostSpirometerSM(ThisEvent);
    ReturnVal = true;
  }
  
  return ReturnVal;
}


/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitSpirometerSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  printf("Spirometer Initialized\n\r");
  
  // Enable peripheral clock to port D
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
    
  // Wait for clock to stabilize
  while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {
      ;
  }
    
  // Enable pin PB5 for digital operation
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT1HI);
    
  // Set PB5 as output 
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT1HI);
  
  ADC_MultiInit(4); 
  ADC_MultiRead(ADCResults);
  BaselineADCResult = ADCResults[0]; 

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState_Spirometer;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSpirometerSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunSpirometerSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  uint8_t NextState; 
  
  
  // Update state of the machine 
  NextState = CurrentState;
  
  // Based on CurrentState of machine, select next state and action 
  switch (CurrentState)
  {
    case InitPState_Spirometer:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        NextState = SpirometerStandby;
        printf("Transitioning to Standby \n\r");
      }
    }
    break;

    case SpirometerStandby:        // If current state is state one
    {
      //printf("In Standby!/n/r");

      if (ThisEvent.EventType == ES_BREATH_DETECTED)
      {
        printf("Whale Blower On!\n\r");
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
        ES_Timer_SetTimer(WHALE_TIMER, 10 * ONE_SEC);
        printf("Timer Started!\n\r");
        ES_Timer_StartTimer(WHALE_TIMER);
        NextState = SpirometerActive;
      }
    }
    break;
    
    case SpirometerActive:
    {
      if(ThisEvent.EventType == ES_TIMEOUT)
      {
        printf("Timeout Received!");
        HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
        NextState = SpirometerStandby; 
      }
    }
    
    default:
      ;
  }                                   // end switch on Current State
  CurrentState = NextState;
  return ReturnEvent;
}


