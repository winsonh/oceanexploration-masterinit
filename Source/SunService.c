/****************************************************************************
 Module
   SunService.c

 Revision
   1.0.1

 Description
  This module implements a state machine for Sun Service

 Notes
  
 History
 When           Who     What/Why
 -------------- ---     --------

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "SunService.h"
#include "ES_Events.h"


/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"


/*----------------------------- Module Defines ----------------------------*/


// 0.8us/tick 
#define ONE_SEC 1000
#define SERVO_0_PULSEWIDTH 938 //pulsewidth
#define SERVO_0_DUTYCYCLE 3
#define SERVO_0_PERIOD 25938
#define SERVO_120_PULSEWIDTH 2186
#define SERVO_5s_DIFFERENCE 104// DIFFERENCE IN PULSEWIDTH & PERIOD
#define SUN_PIN_GROUP 6
#define SUN_PIN 12


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static bool InitSunPWMPorts(void);
static bool TickSun();
static bool InitSunPWMPorts(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t              MyPriority;
static SunState_t CurrentState;
static int CurrentPulseWidth=SERVO_0_PULSEWIDTH;
static int CurrentPeriod=SERVO_0_PERIOD;
static int i=0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitializeSunService

 Parameters

 Returns
  Takes a priority number, returns True.

 Description
     Initializes State Machine
 Notes

 Author
     HyunJoo Lee 2018-11-15 12:37pm
****************************************************************************/

bool InitializeSunService(uint8_t Priority)
{
  // Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
	ES_Event_t ThisEvent;
	
  //Set currentState to InitSun
	CurrentState=InitSun;
		
	//PWM Port Initialization
	if(InitSunPWMPorts()!=true)
  {
		ThisEvent.EventType=ES_ERROR;
		return PostSunService(ThisEvent);		
  }
	//Post Event ES_INIT to SunService queue

	ThisEvent.EventType=ES_INIT;
	PostSunService(ThisEvent);
  return true;
}



/****************************************************************************
 Function
     PostSunService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     HyunJoo Lee 2018-11-15 12:37pm
****************************************************************************/
bool PostSunService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunSunService 

 Parameters
  NextState
 Returns
  The parameter field of the ThisEvent will be the time that the event occurred.
  Returns ES_NO_EVENT
 Description
 
  Set CurrentState to NextState

  Return ES_NO_EVENT
  End of RunSunService
 Notes

 Author
     HyunJoo Lee 2018-11-15 12:33pm
****************************************************************************/
ES_Event_t RunSunService(ES_Event_t ThisEvent)
{
  static SunState_t NextState; 
  ES_Event_t ReturnEvent;

  //Based on the state of the CurrentState variable choose one of the following blocks of code:
  switch (CurrentState)
  {
		case (InitSun):
		{
			//if leaf is inserted, start SunTimer(nead leaf module to post to sunservice
			if(true)//ES_LEAF_INSERTION
      //if(ThisEvent.EventType==ES_LEAF_INSERTION)
			{
				//Take sun to initial place;
        ES_Timer_SetTimer(SUN_TIMER, 5*ONE_SEC);
        printf("\n\rSun5sTimerStarted\n");
        ES_Timer_StartTimer(SUN_TIMER);
				NextState=Ticking;
			}
		}
		case (Ticking):
		{
			if(ThisEvent.EventParam==SUN_TIMER)
			{
        if(CurrentPulseWidth<SERVO_120_PULSEWIDTH)
        {
          //if the clock did not reach 120 degrees
          //add the 5s difference in pulsewidth and period
          //to get next pulse width
          CurrentPulseWidth+=SERVO_5s_DIFFERENCE;
          CurrentPeriod+=SERVO_5s_DIFFERENCE;
        }
        //Tick
        TickSun();
        //PWM_TIVA_SetPeriod(CurrentPeriod, SUN_PIN_GROUP);
        //PWM_TIVA_SetPulseWidth(CurrentPulseWidth, SUN_PIN);
        printf("\n\rTick \r\nCurrentPulseWidth: %d ,\r\nCurrentPulsePeriod: %d",\
        CurrentPulseWidth,CurrentPeriod );
        
        //reset SunTimer5s
        ES_Timer_SetTimer(SUN_TIMER, 5*ONE_SEC);
        ES_Timer_StartTimer(SUN_TIMER);
				NextState=Ticking;
        
			}
			if((ThisEvent.EventType==ES_TIMEOUT)&&(ThisEvent.EventParam==1))
			{
        printf("\r\n ES_TIMEOUT");
        CurrentPulseWidth=SERVO_0_PULSEWIDTH;
        CurrentPeriod=SERVO_0_PERIOD;				
        TickSun();
        //PWM_TIVA_SetPulseWidth(SERVO_0_PULSEWIDTH,SUN_PIN);//0.75ms is 0 degrees, 0.75ms/0.8us =938 
        //PWM_TIVA_SetPeriod(SERVO_0_PERIOD, SUN_PIN_GROUP);
        NextState=InitSun;
			}
			if (ThisEvent.EventType==ES_GAME_OVER) {
				NextState = InitSun;
			}
			if (ThisEvent.EventType==ES_ALL_WON) {
				NextState = InitSun;
			}
		}
  }
  CurrentState = NextState;
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}


/****************************************************************************
 Function
     TickSun
 Parameters

 Returns
  Takes NOTHING returns true if ticked
 Description
	Sets the DutyCycle of the Sun_PIN
  
 Notes
	//Normally called every 5s to tick the sun
 Author
     HyunJoo Lee 2018-11-13 10:55pm

****************************************************************************/

static bool TickSun()
{
  bool ReturnVal=false;
  printf("\n\r Inside TickSun");
  PWM_TIVA_SetPeriod(CurrentPeriod, SUN_PIN_GROUP);
  PWM_TIVA_SetPulseWidth(CurrentPulseWidth, SUN_PIN);
  
	ReturnVal=true;
  return ReturnVal;
}


/****************************************************************************
 Function
     InitSunPWMPorts
 Parameters

 Returns
  Takes nothing, and returns true when initiated
 Description
	Initializes the SUN PWM Ports
  
 Notes

 Author
HyunJoo Lee 2018-11-14 7:34pm
****************************************************************************/

static bool InitSunPWMPorts(void)
{
  printf("\n\r Inside InitSunPWMPorts");
	PWM_TIVA_Init(16); //Initialize all 16 port pints
  //(dutyCycle 0%(0~100), channel #12:PF0)
	PWM_TIVA_SetPulseWidth(SERVO_0_PULSEWIDTH,SUN_PIN);//0.75ms is 0 degrees, 0.75ms/0.8us =938 
  PWM_TIVA_SetPeriod(SERVO_0_PERIOD, 6);
  return true;
}